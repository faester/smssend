using Amazon;
using Amazon.Runtime;
using Amazon.SimpleNotificationService.Model;

namespace SMSSend
{
    public class SmsSender
    {
        private readonly AWSCredentials _credentials;
        private readonly RegionEndpoint _endpoint;
        private readonly string _sender;

        public SmsSender(AWSCredentials credentials, RegionEndpoint endpoint, string sender)
        {
            _credentials = credentials;
            _endpoint = endpoint;
            _sender = sender;
        }

        public void SendSms(string message, SmsRecipient recipient)
        {
            var snsClient = new Amazon.SimpleNotificationService.AmazonSimpleNotificationServiceClient(_credentials, _endpoint);

            var publishRequest = new PublishRequest
            {
                Message = message,
                PhoneNumber = recipient.Phone
            };
            SetSmsType(publishRequest);
            SetMaxPrice(publishRequest, 0.25);
            SetSender(publishRequest);


            snsClient.Publish(publishRequest);
        }

        private void SetSender(PublishRequest publishRequest)
        {
            var sender = new MessageAttributeValue
            {
                StringValue = _sender,
                DataType = "String",
            };
            publishRequest.MessageAttributes["AWS.SNS.SMS.SenderID"] = sender;
        }

        private void SetMaxPrice(PublishRequest publishRequest, double maxPriceUsd)
        {
            var priceString = maxPriceUsd.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            var maxPrice = new MessageAttributeValue
            {
                StringValue = priceString, // Max price USD
                DataType = "Number"
            };
            publishRequest.MessageAttributes.Add("AWS.SNS.SMS.MaxPrice", maxPrice);

        }

        private void SetSmsType(PublishRequest publishRequest)
        {
            publishRequest.MessageAttributes["AWS.SNS.SMS.SMSType"] = new MessageAttributeValue
            {
                StringValue = "Transactional", // Or promotional. Transactional are prioritized (and more expensive).
                DataType = "String", // Or promotional. Transactional are prioritized (and more expensive).
            };
        }
    }
}