﻿using System.Configuration;
using Amazon;
using Amazon.Runtime;

namespace SMSSend
{
    class Program
    {
        static void Main(string[] args)
        {
            //http://docs.aws.amazon.com/sns/latest/dg/sms_publish-to-phone.html

            var accessKey = ConfigurationManager.AppSettings["awskey"];
            var accessSecret = ConfigurationManager.AppSettings["awssecret"];
            AWSCredentials credentials = new BasicAWSCredentials(accessKey, accessSecret);
            RegionEndpoint endpoint = RegionEndpoint.EUWest1;

            var smsSender = new SmsSender(credentials, endpoint, "jppoltest");
            smsSender.SendSms("Hello! This is a test message", SmsRecipient.Create("4529641657"));
        }
    }
}
