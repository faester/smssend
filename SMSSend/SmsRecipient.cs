using System;
using System.Text.RegularExpressions;

namespace SMSSend
{
    public class SmsRecipient
    {
        private static readonly Regex Validation = new Regex("^[0-9]+$");
        public readonly string Phone;

        public SmsRecipient(string phone)
        {
            Phone = phone;
        }

        public static SmsRecipient Create(string phone)
        {
            if (!Validation.IsMatch(phone))
            {
                throw new ArgumentException("Not a valid phone format.", nameof(phone));
            }
            return new SmsRecipient(phone);
        }
    }
}